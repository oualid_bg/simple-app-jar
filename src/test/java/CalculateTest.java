import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculateTest {

    Calculate calculate;

    @Before
    public void setup(){
        calculate = new Calculate();
    }

    @Test
    public void additionTest(){
        Assert.assertEquals(3,calculate.addition(1,2));
        Assert.assertEquals(5,calculate.addition(3,2));
        Assert.assertEquals(4,calculate.addition(1,3));
        Assert.assertEquals(-1,calculate.addition(-2,1));
    }
    @Test
    public void soustractionTest(){
        Assert.assertEquals(-1,calculate.soustraction(1,2));
        Assert.assertEquals(-2,calculate.soustraction(1,3));
        Assert.assertEquals(1,calculate.soustraction(3,2));
        Assert.assertEquals(-3,calculate.soustraction(-2,1));
    }
}
